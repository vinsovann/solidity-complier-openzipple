const mnemonic =
  "wire bullet menu exact make spell cabin will step abuse panel armed";
const HDWalletProvider = require("@truffle/hdwallet-provider");
module.exports = {
  //    contracts_build_directory: "./output",
  networks: {
    ropsten: {
      provider: function () {
        return new HDWalletProvider(
          mnemonic,
          "https://ropsten.infura.io/v3/ec769f8d6857444bbb20e9036d068143"
        );
      },
      network_id: "3",
    },
  },
  compilers: {
    solc: {
      version: "0.8.2",
      docker: false,
    },
  },
  plugins: ["truffle-plugin-verify"],
  api_keys: {
    etherscan: "ASMZSK3B5FVS3P5P6H9B6TEVCDBR1GGSJH",
  },
};
